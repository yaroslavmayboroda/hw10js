'use strict';

function changeTab(tabId) {

    const tabs = document.querySelectorAll('.tab-content');
    tabs.forEach(tab => {
        tab.classList.remove('active-tab');
    });


    const activeTab = document.getElementById(tabId);
    activeTab.classList.add('active-tab');
}